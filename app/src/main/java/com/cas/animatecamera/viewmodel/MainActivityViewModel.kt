package com.cas.animatecamera.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cas.animatecamera.repository.Repository
import com.cas.animatecamera.repository.model.NearestDriverResponse
import com.cas.animatecamera.repository.net.ApiResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MainActivityViewModel @Inject constructor(private val repository: Repository) : ViewModel() {
    private val disposable = CompositeDisposable()
    private val responseLiveData = MutableLiveData<ApiResponse<NearestDriverResponse>>()

    fun nearestDrivers(): MutableLiveData<ApiResponse<NearestDriverResponse>> {
        return responseLiveData
    }

    fun hitGetNearestDrivers(token: String, lat: String, lng: String) {
        disposable.add(repository.executeVerifyCode(token, lat, lng)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { responseLiveData.value = ApiResponse.loading() }
            .subscribe({
                responseLiveData.value = ApiResponse.success(it)
            }, {
                responseLiveData.value = ApiResponse.error(it)
            })
        )

    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}
