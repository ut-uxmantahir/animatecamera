package com.cas.animatecamera.di.component

import com.cas.animatecamera.ui.activity.MainActivity
import com.cas.animatecamera.di.modules.AppModule
import com.cas.animatecamera.di.modules.UtilsModule
import com.cas.animatecamera.di.modules.ViewModelsModule
import com.cas.animatecamera.ui.activity.CityActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [UtilsModule::class, ViewModelsModule::class, AppModule::class]
)
interface AppComponents {

    fun doInjection(activity: MainActivity)
    fun doInjection(activity: CityActivity)
}