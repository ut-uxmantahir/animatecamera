package com.cas.animatecamera.di.modules

import androidx.lifecycle.ViewModelProvider
import com.cas.animatecamera.viewmodel.factory.ViewModelFactory
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelFactoryModule {

    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory
}
