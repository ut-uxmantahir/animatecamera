package com.cas.animatecamera.di.modules

import androidx.lifecycle.ViewModel
import com.cas.animatecamera.di.keys.ViewModelKey
import com.cas.animatecamera.viewmodel.MainActivityViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelsModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainActivityViewModel::class)
    abstract fun bindMainActivityViewModel(viewModel: MainActivityViewModel): ViewModel

}