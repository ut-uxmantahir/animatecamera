package com.cas.animatecamera.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cas.animatecamera.R
import com.cas.animatecamera.databinding.ItemCitiesBinding
import com.cas.animatecamera.repository.model.NearestDriverData
import com.cas.animatecamera.utils.Globals


class CitiesAdapter(
    private val context: Context,
    private val list: ArrayList<NearestDriverData>?
) : RecyclerView.Adapter<CitiesAdapter.ViewHolder>() {

    private val layoutResId: Int @LayoutRes get() = R.layout.item_cities
    private lateinit var binding: ItemCitiesBinding


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        binding = DataBindingUtil.inflate(inflater, layoutResId, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return 22/*list!!.size*/
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        Globals.setImageWithCurvedEdges(context, "url",holder.binding.ivImage)
    }

    inner class ViewHolder(val binding: ItemCitiesBinding): RecyclerView.ViewHolder(binding.root)
}