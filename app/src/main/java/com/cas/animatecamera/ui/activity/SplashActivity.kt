package com.cas.animatecamera.ui.activity

import android.content.Intent
import android.os.Bundle
import com.cas.animatecamera.R
import com.cas.animatecamera.base.BaseActivity
import rx.Observable
import java.util.*
import java.util.concurrent.TimeUnit

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Observable.defer {
            Observable.just<Any?>(null)
                .delay(500, TimeUnit.MILLISECONDS)
        }.doOnNext {
            switchActivity()
        }.subscribe()

    }

    private fun switchActivity() {
            val i = Intent(this@SplashActivity, MainActivity::class.java)
            startActivity(i)
            finish()
    }

    override fun onBackPressed() {

    }

}
