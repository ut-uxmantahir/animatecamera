package com.cas.animatecamera.ui.activity

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import android.provider.Settings
import android.view.animation.Interpolator
import android.view.animation.LinearInterpolator
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.cas.animatecamera.R
import com.cas.animatecamera.base.BaseActivity
import com.cas.animatecamera.base.MyApplication
import com.cas.animatecamera.databinding.ActivityMainBinding
import com.cas.animatecamera.helpers.MapHelper
import com.cas.animatecamera.repository.model.NearestDriverResponse
import com.cas.animatecamera.repository.net.ApiResponse
import com.cas.animatecamera.repository.net.Status
import com.cas.animatecamera.utils.Constant
import com.cas.animatecamera.utils.D
import com.cas.animatecamera.utils.Globals
import com.cas.animatecamera.utils.toast
import com.cas.animatecamera.viewmodel.MainActivityViewModel
import com.cas.animatecamera.viewmodel.factory.ViewModelFactory
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.Polyline
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.single.PermissionListener
import javax.inject.Inject

class MainActivity : BaseActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private val TAG = this.javaClass.name
    private val layoutResID: Int @LayoutRes get() = R.layout.activity_main
    private lateinit var binding: ActivityMainBinding

    @Inject
    lateinit var factory: ViewModelFactory
    lateinit var viewModel: MainActivityViewModel

    private lateinit var gMap: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    var polyline: Polyline? = null
    private lateinit var currentLatLng: LatLng
    private lateinit var selectedLatLng: LatLng
    private var latLngList = mutableListOf<LatLng>()
    private var mPosition: Int? = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        switchActivity()
        setupDataBinding()
        initViews()
        setInjection()
        setViewModel()
//        hitGetNearestDrivers()
        initializeGoogleMaps()
    }

    private fun switchActivity() {
//        val i = Intent(this@MainActivity, MapsActivty::class.java)
//        startActivity(i)
//        finish()
    }

    private fun initViews(){
        binding.btnSwitch.setOnClickListener {

        startActivity(
            Intent(this@MainActivity, CityActivity::class.java)
            .apply {

            })

        }
    }


    private fun setupDataBinding() {
        binding = DataBindingUtil.setContentView(this@MainActivity, layoutResID)
    }

    private fun setInjection() {
        MyApplication.getAppComponent(this@MainActivity).doInjection(this)
    }

    private fun setViewModel() {
        viewModel = ViewModelProvider(this, factory).get(MainActivityViewModel::class.java)
        viewModel.nearestDrivers().observe(this, {
            consumeResponse(it)
        })
    }

    private fun initializeGoogleMaps() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

    }

    private fun hitGetNearestDrivers(){

//        val params = HashMap<String, String>()
//        params["token"] = userData.getToken().toString()

        val token = getString(R.string.token)

        viewModel.hitGetNearestDrivers(token, currentLatLng.latitude.toString(), currentLatLng.longitude.toString())
    }

    private fun consumeResponse(apiResponse: ApiResponse<NearestDriverResponse>?) {
        D.d(TAG, "consumeResponse: " + apiResponse!!.data)
        when (apiResponse.status) {
            Status.LOADING -> {
                Globals.showProgressDialog(this)
            }
            Status.SUCCESS -> {
                D.d(TAG, "consumeResponse SUCCESS: " + apiResponse.data)
                Globals.hideProgressDialog()
                renderSuccessResponse(apiResponse.data as NearestDriverResponse)
            }
            Status.ERROR -> {
                D.d(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                Globals.hideProgressDialog()
                Globals.showAlertMessage(this, apiResponse.error.toString())
            }
            else -> {
            }
        }
    }

    private fun renderSuccessResponse(response: NearestDriverResponse) {
        if (response.status == Constant.RESPONSE_CODE_SUCCESS) {
            D.d(TAG, "renderSuccessResponse SUCCESS: " + response.data)
            toast(this@MainActivity,"Success")


            response.data.driver.let { driver->
                driver.forEach {
                    latLngList.add(LatLng(it.lat.toDouble(), it.lng.toDouble()))
                }
            }

            latLngList.add(0, LatLng(response.data.passenger.lat.toDouble(), response.data.passenger.lng.toDouble()))
//            latLngList.add()

        } else {
            Globals.showAlertMessage(this, response.msg)
        }
    }


    override fun onMapReady(googleMap: GoogleMap?) {
        if (googleMap != null) {
            gMap = googleMap
            askPermissions()
        }
    }

    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri: Uri = Uri.fromParts("package", this.packageName, null)
        intent.data = uri
        startActivity(intent)
    }

    private fun askPermissions() {
        Dexter.withContext(this)
            .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                    initMap()
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: com.karumi.dexter.listener.PermissionRequest?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                    if (response?.isPermanentlyDenied!!) {
                        openSettings()
                    } else {
                        toast(this@MainActivity, "Location permission required to use maps")
                    }
                }
            }).check()
    }

    @SuppressLint("MissingPermission")
    private fun initMap() {
        gMap.isMyLocationEnabled = true
        gMap.uiSettings.isMyLocationButtonEnabled = false
        fusedLocationClient.lastLocation.addOnSuccessListener {
            if (it != null) {
                gMap.clear()
                currentLatLng = LatLng(it.latitude, it.longitude)

                hitGetNearestDrivers()
                latLngList.add(LatLng(it.latitude, it.longitude))
                latLngList.add(LatLng(it.latitude - 0.003, it.longitude - 0.005))
                latLngList.add(LatLng(it.latitude - 0.004, it.longitude - 0.004))
                latLngList.add(LatLng(it.latitude - 0.005, it.longitude - 0.003))

                MapHelper.addMarker(0, gMap,currentLatLng)
                animateMarker(1, latLngList[1], latLngList[2], false)
                animateMarker(2, latLngList[2], latLngList[3], false)
                animateMarker(3, latLngList[3], latLngList[0], false)

                gMap.setOnMarkerClickListener(this)
            }
        }
    }

    private fun animateMarker(position: Int, startPosition: LatLng, toPosition: LatLng, hideMarker: Boolean ) {

        val marker = MapHelper.addMarker(position, gMap, startPosition)


        val handler = Handler(Looper.getMainLooper())
        val start = SystemClock.uptimeMillis()

        val duration: Long = 7000
        val interpolator: Interpolator = LinearInterpolator()

        handler.post(object : Runnable {
            override fun run() {
                val elapsed = SystemClock.uptimeMillis() - start
                val t = interpolator.getInterpolation(
                    elapsed.toFloat() / duration
                )
                val lng: Double = t * toPosition.longitude + (1 - t) * startPosition.longitude
                val lat: Double = t * toPosition.latitude + (1 - t) * startPosition.latitude
                marker.position = LatLng(lat, lng)

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16)
                } else {
                    marker.isVisible = !hideMarker
                }
            }
        })
    }

    override fun onMarkerClick(marker: Marker?): Boolean {
        mPosition = marker?.tag as? Int

        mPosition.let {
            selectedLatLng = latLngList[mPosition!!]
        }

        polyline?.remove()

        polyline = MapHelper.addPolyLineToMap(gMap, currentLatLng, selectedLatLng)

        return true
    }


}