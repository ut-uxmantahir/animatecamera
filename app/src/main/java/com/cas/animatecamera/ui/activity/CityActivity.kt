package com.cas.animatecamera.ui.activity

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.cas.animatecamera.R
import com.cas.animatecamera.base.BaseActivity
import com.cas.animatecamera.base.MyApplication
import com.cas.animatecamera.databinding.ActivityCityBinding
import com.cas.animatecamera.ui.adapters.CitiesAdapter
import com.cas.animatecamera.viewmodel.MainActivityViewModel
import com.cas.animatecamera.viewmodel.factory.ViewModelFactory
import javax.inject.Inject

class CityActivity : BaseActivity() {
    private val TAG = this.javaClass.name
    private val layoutResID: Int @LayoutRes get() = R.layout.activity_city
    private lateinit var binding: ActivityCityBinding

    @Inject
    lateinit var factory: ViewModelFactory
    lateinit var viewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupDataBinding()
        setInjection()
        setViewModel()
        setRecyclerView()
    }

    private fun setupDataBinding() {
        binding = DataBindingUtil.setContentView(this@CityActivity, layoutResID)
    }

    private fun setInjection() {
        MyApplication.getAppComponent(this@CityActivity).doInjection(this)
    }

    private fun setViewModel() {
//        viewModel = ViewModelProvider(this, factory).get(MainActivityViewModel::class.java)
//        viewModel.nearestDrivers().observe(this, {
//            consumeResponse(it)
//        })
    }

    private fun setRecyclerView(){
        val gridLayoutManager = GridLayoutManager(
            applicationContext,
            2,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        binding.rvCities.layoutManager = gridLayoutManager
        binding.rvCities.adapter = CitiesAdapter(this, null)
    }

}