package com.cas.animatecamera.repository.net

import com.cas.animatecamera.repository.model.*
import retrofit2.http.*
import io.reactivex.Observable

interface ApiCallInterface {

    @FormUrlEncoded
    @POST(Urls.NEAREST_DRIVERS)
    fun getNearestDrivers(
        @Field("token") token: String?,
        @Field("lng") lat: String?,
        @Field("lng") lng: String?
    ): Observable<NearestDriverResponse>

}
