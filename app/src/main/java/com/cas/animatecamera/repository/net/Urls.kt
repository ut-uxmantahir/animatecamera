package com.cas.animatecamera.repository.net

class Urls {
    companion object {

        const val BASE_URL = "http://54.254.32.130/api/v1/" // live-stage

        //api end points

        //nearest driver
        const val NEAREST_DRIVERS = "getNearestDrivers"
    }
}