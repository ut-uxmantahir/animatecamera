package com.cas.animatecamera.repository

import com.cas.animatecamera.repository.model.NearestDriverResponse
import com.cas.animatecamera.repository.net.ApiCallInterface
import io.reactivex.Observable
import retrofit2.http.Field
import retrofit2.http.Header

class Repository(private val apiCallInterface: ApiCallInterface) {

    fun executeVerifyCode(
        @Field("token") token: String?,
        @Field("lat") lat: String?,
        @Field("lng") lng: String?
//        @Field("token") token: String
    ): Observable<NearestDriverResponse> {
        return apiCallInterface.getNearestDrivers(token, lat, lng)
    }

}