package com.cas.animatecamera.repository.net;

public enum Status {
    LOADED,
    LOADING,
    SUCCESS,
    ERROR
}