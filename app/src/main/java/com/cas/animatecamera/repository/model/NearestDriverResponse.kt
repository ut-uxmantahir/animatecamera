package com.cas.animatecamera.repository.model

data class NearestDriverResponse(
    val status: Int,
    val msg: String,
    val data: NearestDriverData
)

data class NearestDriverData(
    val driver: ArrayList<DriverData>,
    val passenger: PassengerData
)

data class DriverData(

    val id: Int,
    val driver_id: Int,
    val lat: String,
    val lng: String,
    val bearing: Long,
    val decode_path: String,
    val current_arrival_time: String,
    val current_distance: String,
    val city: String,
    val area_name: String,
    val vehicle_cat_id: Int,
    val dual_vehicle_cat_id: Int,
    val status: Int,
    val created_at: String,
    val updated_at: String,
    val distance: Long,
)

data class PassengerData(
    val lat: String,
    val lng: String
)

