package com.cas.animatecamera.base

import android.content.Context
import androidx.multidex.MultiDexApplication
import com.cas.animatecamera.di.component.AppComponents
import com.cas.animatecamera.di.component.DaggerAppComponents
import com.cas.animatecamera.di.modules.AppModule

class MyApplication : MultiDexApplication() {
    lateinit var component: AppComponents

    override fun onCreate() {
        super.onCreate()
        component = DaggerAppComponents.builder().appModule(AppModule(this)).build()
    }

    companion object {

        fun getAppComponent(context: Context): AppComponents {
            return (context.applicationContext as MyApplication).component
        }
    }

}