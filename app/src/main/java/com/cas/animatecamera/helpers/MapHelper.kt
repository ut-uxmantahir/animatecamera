package com.cas.animatecamera.helpers

import android.graphics.Color
import com.cas.animatecamera.R
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*

object MapHelper {

    fun addMarker(position: Int, gMap: GoogleMap, startPosition: LatLng): Marker {
        val marker: Marker

        if (position == 0){
            marker = gMap.addMarker(
                MarkerOptions()
                    .anchor(0.5f, 0.5f)
                    .position(startPosition)
                    .title("You")
                    .snippet("Your current position")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
            )
        }
        else{
            marker = gMap.addMarker(
                MarkerOptions()
                    .anchor(0.5f, 0.5f)
                    .position(startPosition)
                    .title("Vehicle $position")
                    .snippet("Book your ride $position")
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car))
            )
        }

        marker.tag = position
        gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(startPosition, 15f))

        return marker
    }

    fun addPolyLineToMap(
        googleMap: GoogleMap,
        currentLatLng: LatLng,
        selectedLatLng: LatLng
    ): Polyline {
        return googleMap.addPolyline(
            PolylineOptions()
                .clickable(true)
                .add(
                    currentLatLng,
                    selectedLatLng
                )
                .endCap(RoundCap())
                .width(10.0f)
                .color(Color.BLUE)
                .jointType(JointType.ROUND)
                .geodesic(true)

        )
    }

    fun removeMarker(marker: Marker?) {
        marker.let {
            marker?.remove()
        }
    }
}


