package com.cas.animatecamera.utils

object Constant {

    const val RESPONSE_CODE_SUCCESS = 200
    const val RESPONSE_CODE_SUCCESS_WITH_EMPTY_ARRAY = 400
    const val RESPONSE_CODE_INVALID_USER_TOKEN = 401
}