package com.cas.animatecamera.utils

import android.app.Activity
import android.content.Context
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.cas.animatecamera.R

object Globals {

    private var progressDialog: AlertDialog? = null

    fun showProgressDialog(context: Context) {

        progressDialog = AlertDialog.Builder(context)
            .setView(R.layout.loading_dialog)
            .setCancelable(false)
            .show()
    }

    fun hideProgressDialog() {
        progressDialog?.let {
            if(it.isShowing)
                it.dismiss()
        }
    }

    fun showAlertMessage(context: Context, message: String) {
        val dialogBuilder = AlertDialog.Builder(context)
        dialogBuilder.setMessage(message)
        dialogBuilder.setCancelable(false)
        dialogBuilder.setPositiveButton(context.getString(R.string.ok)) { dialog, which -> }
        if (!(context as Activity).isFinishing) {
            dialogBuilder.create().show()
        }
    }

    fun setImageWithCurvedEdges(context: Context, url: String, imageView: ImageView) {
        val circularProgressDrawable = CircularProgressDrawable(context)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.start()

        Glide.with(context)
            .load(R.drawable.ic_sample_bg)
            .placeholder(circularProgressDrawable)
            .transform(CenterCrop(), RoundedCorners(24))
            .into(imageView)
    }


}